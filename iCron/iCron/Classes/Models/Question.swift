//
//  Question.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class Option: NSObject {
    var id: Int
    var title: String
    
    init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
    
    class func defaultOptions() -> [Option] {
        let yes = Option.init(id: 0, title: "Да")
        let maybe = Option.init(id: 0, title: "Затрудняюсь ответить")
        let no = Option.init(id: 0, title: "Нет")
        
        return [yes, maybe, no]
    }
}

class Question: NSObject {

    var title: String!
    var correctId: Int!
    var options: [Option] = []
    
    class func createWith(title: String) -> Question {
        let question = Question.init()
        question.title = title
        question.options = Option.defaultOptions()
        
        return question
    }
    
}
