//
//  StartSelectViewController.swift
//  iCron
//
//  Created by Александр on 23/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class StartSelectViewController: UIViewController {

    @IBOutlet var applicantButton: UIButton! {
        didSet {
            self.applicantButton.layer.cornerRadius = self.applicantButton.frame.height / 2
            self.applicantButton.layer.masksToBounds = true
        }
    }
    @IBOutlet var employerButton: UIButton! {
        didSet {
            self.employerButton.layer.cornerRadius = self.employerButton.frame.height / 2
            self.employerButton.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDidActionTapped(_ sender: Any) {
        if let delegate = UIApplication.shared.delegate, let window = delegate.window {
            let storyboard = UIStoryboard.init(name: "Authorize", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC")
            let navigation = UINavigationController.init(rootViewController: controller)
            navigation.setNavigationBarHidden(true, animated: false)
            window?.rootViewController = navigation
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
