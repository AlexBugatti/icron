//
//  StartVC.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class StartVC: UIViewController {

    @IBOutlet var nextButton: UIButton! {
        didSet {
            self.nextButton.layer.cornerRadius = self.nextButton.frame.height / 2
            self.nextButton.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
