//
//  SignInVC.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {

    @IBOutlet var signButton: UIButton! {
        didSet {
            self.signButton.layer.cornerRadius = self.signButton.frame.height / 2
            self.signButton.layer.masksToBounds = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDidAuthTapped(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Account", bundle: nil)
        let account = storyboard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
        let navigation = UINavigationController.init(rootViewController: account)
        navigation.setNavigationBarHidden(true, animated: false)
        if let delegate = UIApplication.shared.delegate, let window = delegate.window {
            window?.rootViewController = navigation
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
