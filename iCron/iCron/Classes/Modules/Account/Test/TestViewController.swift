//
//  TestViewController.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    var items: [Question] = []
    var currentIndex: Int = 0
    @IBOutlet var titleQuestionLabel: UILabel!
    @IBOutlet var backButton: UIButton! {
        didSet {
            self.backButton.layer.cornerRadius = self.backButton.frame.height / 2
            self.backButton.layer.masksToBounds = true
        }
    }
    
    
    @IBOutlet var collectionView: UICollectionView! {
        didSet {
            self.collectionView.register(UINib.init(nibName: "QuestionCell", bundle: nil), forCellWithReuseIdentifier: "QuestionCell")
            self.collectionView.isPagingEnabled = true
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.currentIndex = 0
        let q1 = Question.createWith(title: "Работа, связанная с учетом и контролем, – это довольно скучно")
        let q2 = Question.createWith(title: "Я предпочту заниматься финансовыми операциями, а не, например, музыкой")
        let q3 = Question.createWith(title: "Невозможно точно рассчитать, сколько времени уйдет на дорогу до работы, по крайней мере, мне")
        let q4 = Question.createWith(title: "Я часто рискую")
        let q5 = Question.createWith(title: "Меня раздражает беспорядок")
        let q6 = Question.createWith(title: "Я охотно почитал(а) бы на досуге о последних достижениях в различных областях науки")
        
//        let q7 = Question.createWith(title: "Работа, связанная с учетом и контролем, – это довольно скучно")
//        let q8 = Question.createWith(title: "Я предпочту заниматься финансовыми операциями, а не, например, музыкой")
//        let q9 = Question.createWith(title: "Невозможно точно рассчитать, сколько времени уйдет на дорогу до работы, по крайней мере, мне")
//        let q10 = Question.createWith(title: "Я часто рискую")
//        let q11 = Question.createWith(title: "Меня раздражает беспорядок")
//        let q12 = Question.createWith(title: "Я охотно почитал(а) бы на досуге о последних достижениях в различных областях науки")
        // Do any additional setup after loading the view.
        
        self.items = [q1,q2,q3,q4,q5,q6]
        self.titleQuestionLabel.text = self.items[self.currentIndex].title

        self.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Тестирование"
    }
    
    @IBAction func onDidNext(_ sender: Any) {
        self.prevQuestion()
    }
    
    func nextQuestion() {
        self.titleQuestionLabel.text = self.items[self.currentIndex].title
        self.currentIndex = self.currentIndex + 1
        self.collectionView.reloadData()
    }
    
    func prevQuestion() {
        if self.currentIndex > 0 {
            self.currentIndex -= 1
        }
        self.titleQuestionLabel.text = self.items[self.currentIndex].title
        self.collectionView.reloadData()
    }
    
    func prepareForNext() {
        if self.currentIndex < (self.items.count - 1) {
            self.nextQuestion()
        } else {
            self.performSegue(withIdentifier: "toResult", sender: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TestViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
        let item = self.items[self.currentIndex]
        cell.configure(question: item)
        cell.didSelectTapped = {
            self.prepareForNext()
        }
        
        return cell
    }
    
}

extension TestViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.002
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.001
    }
}
