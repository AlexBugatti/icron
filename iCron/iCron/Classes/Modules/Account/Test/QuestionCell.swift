//
//  QuestionCell.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class QuestionCell: UICollectionViewCell {

    var currentQuestion: Question!
    var didSelectTapped: (()->())?
    @IBOutlet var tableView: UITableView! {
        didSet {
            self.tableView.register(UINib.init(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: "OptionCell")
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = 60
        }
    }
    
    func configure(question: Question) {
        self.currentQuestion = question
        self.tableView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension QuestionCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectTapped?()
    }
    
}

extension QuestionCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentQuestion != nil {
             return self.currentQuestion.options.count
        }
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionCell
        let option = self.currentQuestion.options[indexPath.row]
        cell.configure(option: option)
        
        return cell
    }

}
