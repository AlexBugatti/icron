//
//  ResultScoreCell.swift
//  iCron
//
//  Created by Александр on 23/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class ResultScoreCell: UITableViewCell {

    
    
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var coverView: UIView! {
        didSet {
            self.coverView.layer.cornerRadius = 10
            self.coverView.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
