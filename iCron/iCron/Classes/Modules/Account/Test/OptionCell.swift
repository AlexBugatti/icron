//
//  OptionCell.swift
//  iCron
//
//  Created by Александр on 22/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class OptionCell: UITableViewCell {

    @IBOutlet var checkView: UIImageView! {
        didSet {
            self.checkView.layer.cornerRadius = self.checkView.frame.width / 2
            self.checkView.layer.borderColor = UIColor.gray.cgColor
            self.checkView.layer.borderWidth = 1
            self.checkView.layer.masksToBounds = true
        }
    }
    @IBOutlet var optionTitleLabel: UILabel!
    var current: Option!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(option: Option) {
        self.current = option
        self.optionTitleLabel.text = option.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onDidActionTapped(_ sender: Any) {
    }
    
    
}
