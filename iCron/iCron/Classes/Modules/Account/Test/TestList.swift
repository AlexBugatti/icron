//
//  TestList.swift
//  iCron
//
//  Created by Александр on 23/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class Test: NSObject {
    var title: String
    var descriptionString: String
    
    init(title: String, descriptionString: String) {
        self.title = title
        self.descriptionString = descriptionString
    }
    
    class func list() -> [Test] {
        let java = Test.init(title: "Java", descriptionString: "этот тест на знание java разработки")
        
        return [java]
    }
}

class TestList: UIViewController {

    var tests: [Test] = []
    @IBOutlet var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(UINib.init(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: "OptionCell")
            self.tableView.estimatedRowHeight = 50
            self.tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Выбор теста"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tests = Test.list()
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TestList: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toTest", sender: nil)
    }
    
}

extension TestList: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionCell
        
        return cell
        
    }
    
    
}
