//
//  ResultVC.swift
//  iCron
//
//  Created by Александр on 23/06/2019.
//  Copyright © 2019 Alexander Bugaev. All rights reserved.
//

import UIKit

class ResultVC: UIViewController {

    var result: [String] = ["Программист-разработчик\nКонкуренция в Омске: 2 человека на место\nКонкуренция в СФО: 5 человека на место\nКонкуренция в России: 8 человека на место\nСслыка на курс",
                            "Тестировщик\nКонкуренция в Омске: 3 человека на место\nКонкуренция в СФО: 6 человека на место\nКонкуренция в России: 9 человека на место\n<ссылка_на_курс>",
                            "Системный администратор\nКонкуренция в Омске: 4 человека на место\nКонкуренция в СФО: 7 человека на место\nКонкуренция в России: 11 человека на место\n<ссылка_на_курс>",
                            "Аналитик\nКонкуренция в Омске: 4 человека на место\nКонкуренция в СФО: 7 человека на место\nКонкуренция в России: 11 человека на место\n<ссылка_на_курс>",
                            "Инженер-проектировщик\nКонкуренция в Омске: 1 человек на место\nКонкуренция в СФО: 2 человек на место\nКонкуренция в России: 4 человек на место\n<ссылки_на_университеты>",
                            "Аудитор\nКонкуренция в Омске: 2 человека на место\nКонкуренция в СФО: 4 человека на мест\nКонкуренция в России: 7 человека на место\n<ссылка_на_курсы>"]
    @IBOutlet var tableView: UITableView! {
        didSet {
            self.tableView.register(UINib.init(nibName: "ResultScoreCell", bundle: nil), forCellReuseIdentifier: "ResultScoreCell")
            self.tableView.register(UINib.init(nibName: "ResultCell", bundle: nil), forCellReuseIdentifier: "ResultCell")
            self.tableView.estimatedRowHeight = 50
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDidDismissTapped(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ResultVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : self.result.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let header = tableView.dequeueReusableCell(withIdentifier: "ResultScoreCell", for: indexPath) as! ResultScoreCell
            header.resultLabel.text = "Вы показали высокий интерес к знаковым системам – это условные знаки, цифры, коды, естественные и искусственные языки. Вы могли бы найти себя в профессиях, связанных с созданием и оформлением документов (на родном или иностранном языке), делопроизводством, анализом текстов и их преобразованием, перекодированием ( секретарь, технический редактор, нотариус); числами(экономист, программист, бухгалтер, математик). Вам интересна кажущаяся многим скучной и монотонной работа с бумагами, цифрами, буквами, документами и т.п. – организация, упорядочивание, анализ, контроль. Вы принимаете решение, тщательно рассмотрев ситуацию и взвесив альтернативы, что делает вас незаменимым в бизнесе, управлении, науках."
            return header
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! ResultCell
            let item = self.result[indexPath.row]
            cell.configure(string: item)
            
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0 {
//            return "От 49 до 60 баллов"
//        } else {
//            return "Востребованные вакансии 2019 года которые вам подойдут:"
//        }
//    }
    
}
